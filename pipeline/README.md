# Pipeline for competition

Папка содержит пайплаин обучения модели.
Рассмотрен классификатор изображений. Цель - находить размытые фото.


Содержание:
- configs - конфиги для обучения
- data - сюда будут загружены файлы с соревнования из Kaggle. Загрузка из https://wandb.ai
- notebooks - находятся .ipynb
- outs - файлы, получающиеся в результате обучения (лучшие веса), результаты работы модели submission.csv
- src - основные скрипты
- wandb - логи
- makefile - для быстрого запуска: 
    - make - загрузить датасет с W&B и запустить обучение, 
    - make train - обучить модель,
    - make validate - вывести roc_auc для валидационного датасета, 
    - make predict - создать и сохранить submission.csv, 
    - make sweep - подбор лучших гиперпараметров с помощью W&B
    - clean - удаление всех созданных во время обучения папок: data, outs, wandb

Посмотреть логи: https://wandb.ai/balakinakate2022/pipeline_competition/overview?workspace=user-balakinakate2022

Проведены эксперименты с различными значениями learning rate и batch_size, используя sweep от WandB. Результаты можно видеть на https://wandb.ai/balakinakate2022/pipeline_competition/runs/mf7wze6n?workspace=user-balakinakate2022

Выбрать гиперпараметры можно исходя из этих экспериментов, взяв те, которые доставляют больший roc_auc.
